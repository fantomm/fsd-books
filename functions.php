<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

/**
 * Set CSRF token to session
 * @return string
 */
    function setupCsrfToken()
    {
        if(!isset($_SESSION['token'])) {
            $_SESSION['token'] = generateCsrfToken();
        }
        return $_SESSION['token'];
    }


/**
 * Generating CSRF token
 * @return string
 */
    function generateCsrfToken()
    {
        $cStrong = True;
        $token = bin2hex(openssl_random_pseudo_bytes(64, $cStrong));
        return $token;
    }


/**
 * Login to database
 * @return DB
 */
    function loginToDb()
    {
        return new DB();
    }


/**
 * Check if user logged in
 * @param $db
 * @return bool || userId
 */
    function checkIfLoggedIn($db)
    {
        $userId = Login::isLoggedIn($db);
        return $userId;
    }


/**
 * Deny access if user not logged in
 * @param $db
 */
    function denyAccessIfNotLoggedIn($db)
    {
        if(!checkIfLoggedIn($db)) {
            echo'{ "Error" : "Unauthorized access! User not logged in!" }';
            http_response_code(401); // 401 - Unauthorized
            exit;
        }
    }


/**
 * Deny Book Delete if user does't have Admin privileges
 * @param $db
 */
    function denyAccessIfNotAdmin($db)
    {
        $userId = checkIfLoggedIn($db);

        if (!$db->query('SELECT u.id as userId FROM users u LEFT JOIN roles r ON u.role_id = r.id WHERE u.id = :id AND r.type = :type ', array(':id' =>  $userId, ':type' => 'admin'))) {
            echo '{ "Error" : "Only Admin has permission to delete a book!" }';
            http_response_code(403); // 403 - Forbiden
            exit;
        }
    }


/**
 * Deny Access if CSRF token is missing or is invalid
 * @param null $csrfToken
 */
    function denyAccessIfCsrfTokenInvalid($csrfToken = null)
    {
        if( !isset($_SESSION['token']) || !isset($csrfToken) || (isset($_SESSION['token']) && isset($csrfToken) && $csrfToken != $_SESSION['token'])) {
            echo'{ "Error" : "Unauthorized access! Invalid csrf token!" }';
            http_response_code(401); // 401 - Unauthorized  Token
            exit;
        }
    }


/**
 * Check if correct password supplied for entered username
 * @param $db
 * @param string $password
 * @param string $username
 */
    function validatePassword($db, $password = '', $username = '')
    {
        if (!password_verify($password, $db->query('SELECT password FROM users WHERE username=:username', array(':username' =>  $username))[0]['password'])) {
            echo '{ "Error" : "Invalid Username and/or Password!" }';
            http_response_code(401); // 401 - Unauthorized
            exit;
        }
    }


/**
 * Check if username exists in database
 * @param $db
 * @param $username
 */
    function validateUsername($db, $username)
    {
        if (!$db->query('SELECT username FROM users WHERE username=:username', array(':username' => $username))) {
            echo '{ "Error" : "Invalid Username and/or Password!" }';
            http_response_code(401); // 401 - Unauthorized
            exit;
        }
    }


/**
 * Get All Allowed Methods for a Single Specified Route
 * @param string $route
 * @return mixed|string
 */
    function getMethodsAllowedForRoute($route = '')
    {
        $allRoutesAndMethodsAllowed = array(
            'books' => array('POST', 'GET', 'PUT', 'DELETE'),
            'auth'  => array('POST'),
            'users' => array('POST')
        );
        $allowedMethodsForRoute = (isset($allRoutesAndMethodsAllowed[$route])) ? $allRoutesAndMethodsAllowed[$route] : '';

        return $allowedMethodsForRoute;
    }


/**
 * Prevent SQL Injection (for string values)
 *
 * @param $variable
 * @return string
 */
    function preventInjectionForStrings($variable)
    {
        $variable = htmlspecialchars($variable);
        $variable = strip_tags($variable);
        return stripslashes($variable);
    }


/**
 * Validate Book Data sent from a form
 * @param $formData
 * @return boolean
 */
    function validateBookData($formData = array())
    {
        // All fields required
        if (empty($formData[':title']) || empty($formData[':author']) || empty($formData[':published']) || empty($formData[':language']) || empty($formData[':language_original'])) {
            echo '{ "Error" : "All the fields must be entered!" }';
            http_response_code(400); // 400 - Bad Request
            exit;
        }

        // Allow only specific characters for these fields
        $filter = '/^[a-z A-Z0-9-_?!.,\'"]+$/';
        if (!preg_match($filter, $formData[':title']) || !preg_match($filter, $formData[':author']) || !preg_match($filter, $formData[':language']) || !preg_match($filter, $formData[':language_original'])) {
            echo '{ "Error" : "Forbidden characters used in text fields!" }';
            http_response_code(409); // 409 - Conflict
            exit;
        }

        // Allow only 4 numeric character for Date Published field
        if (!preg_match('/^\d{4}$/', $formData[':published'])) {
            $currYear = date('Y');
            if($formData[':published'] < 1000 || $formData[':published'] > $currYear) {
                echo '{ "Error" : "Year must be between 1000. and '. $currYear .'.!" }';
                http_response_code(409); // 409 - Conflict
                exit;
            }
            echo '{ "Error" : "Year must contain only 4 numeric characters!" }';
            http_response_code(409); // 409 - Conflict
            exit;
        }

        return true;
    }


/**
 * Validate User Data sent from a form
 * @param $db
 * @param $formData
 * @return mixed
 */
    function validateUserData($db, $formData = array())
    {
        if ($db->query('SELECT username FROM users WHERE username=:username', array(':username' => $formData[':username']))) {
            echo '{ "Error" : "Username already Exists! Choose another one!" }';
            http_response_code(409); // 409 - Conflict
            exit;
        }

        if ($db->query('SELECT email FROM users WHERE email=:email', array(':email' => $formData[':email']))) {
            echo '{ "Error" : "Email already in use!" }';
            http_response_code(409); // 409 - Conflict
            exit;
        }

        if (strlen($formData[':username']) < 3 || strlen($formData[':username']) > 50) {
            echo '{ "Error" : "Invalid Username! Must be between 3 and 50 characters!" }';
            http_response_code(409); // 409 - Conflict
            exit;
        }

        if (!preg_match('/^[A-Za-z0-9]*$/', $formData[':username'])) {
            echo '{ "Error" : "Invalid Username! Must contain only big and/or small characters and/or numbers!" }';
            http_response_code(409); // 409 - Conflict
            exit;
        }

        if (strlen($formData[':password']) < 3 || strlen($formData[':password']) > 60) {
            echo '{ "Error" : "Invalid Password! Must be between 3 and 60 characters!" }';
            http_response_code(409); // 409 - Conflict
            exit;
        }

        if (!filter_var($formData[':email'], FILTER_VALIDATE_EMAIL)) {
            echo '{ "Error" : "Invalid Email! Please provide a valid email!" }';
            http_response_code(409); // 409 - Conflict
            exit;
        }

        if (empty($formData[':role'])) {
            echo '{ "Error" : "Role must be chosen!" }';
            http_response_code(409); // 409 - Conflict
            exit;
        }

        return true;
    }


/**
 * @param $search
 * @param $offset
 * @param $per_page
 * @return array
 */
    function generateGetBooksQuery($search, $offset, $per_page)
    {
        $query = "SELECT * FROM books";

        $where = '';
        if (!empty($search['author'])) {
            $author = preventInjectionForStrings($search['author']);
            $where = (!empty($where)) ? $where .= " AND" : '';
            $where .= " author LIKE '%{$author}%'";
        }

        if (!empty($search['published'])) {
            $published = intval($search['published']);
            $where = (!empty($where)) ? $where .= " AND" : '';
            $where .= " published = '{$published}'";
        }

        if (!empty($where)) {
            $query .= " WHERE{$where}";
        }

        if (!empty($search['filterBy'])) {
            $ascDesc = preventInjectionForStrings($search['filterBy']);
            $query .= " ORDER BY published {$ascDesc}";
        }

        $query .= " LIMIT {$offset},{$per_page}";
        return array($query, $where);
    }


/**
 * Set the Auth token to Cookie
 * @param $token
 */
    function setCookies($token)
    {
        setcookie("FSDID", $token, time() + 60 * 60 * 24 * 7, '/', NULL, NULL, TRUE); // 7 days
        setcookie("FSDID_",  '1' , time() + 60 * 60 * 24 * 3, '/', NULL, NULL, TRUE); // 3 days (refresh token)
    }


/**
 * Unset the Auth Cookies
 */
    function unsetCookies()
    {
        setcookie('FSDID',  '1', time() - 3600);
        setcookie('FSDID_', '1', time() - 3600);
    }


/**
 * Deny access if Auth token from cookie missing, invalid or expired
 * @param $db
 */
    function validateAuthCookieToken($db)
    {
        if (!isset($_COOKIE['FSDID'])) {
            echo '{ "Error": "Already logged out!" }';
            http_response_code(400); // 400 - Bad Request
            exit;
        }

        if (!$db->query('SELECT token FROM login_tokens WHERE token=:token', array(':token' => sha1($_COOKIE['FSDID'])))) {
            echo '{ "Error": "Invalid Auth token!" }';
            http_response_code(400); // 400 - Bad Request
            exit;
        }
    }