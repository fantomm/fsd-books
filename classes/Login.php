<?php

class Login
{
    public static function isLoggedIn($db)
    {
        if(isset($_COOKIE['FSDID']))
        {
            if($db->query('SELECT user_id FROM login_tokens WHERE token=:token', array(':token' => sha1($_COOKIE['FSDID']))))
            {
                $userId = $db->query('SELECT user_id FROM login_tokens WHERE token=:token', array(':token' => sha1($_COOKIE['FSDID'])))[0]['user_id'];

                if(isset($_COOKIE['FSDID_'])) {
                    return $userId;
                }
                else { // if "refresh" token expired generate new tokens
                    $token = generateCsrfToken();
                    $db->query('INSERT INTO login_tokens VALUES (null, :token, :user_id)', array(':token' => sha1($token), ':user_id' => $userId));
                    $db->query('DELETE FROM login_tokens WHERE token=:token', array(':token' => sha1($_COOKIE['FSDID'])));

                    setCookies($token);
                }
                return $userId;
            }
        }
        return false;
    }
}