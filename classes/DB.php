<?php

class DB {

    private $pdo;

    public function __construct()
    {
        $pdo = new PDO('mysql:host='. HOST .';dbname='. DBNAME .';charset:utf8', USERNAME, PASSWORD);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo = $pdo;
    }

    public function query($query, $params = array())
    {
        $statement = $this->pdo->prepare($query);
        $statement->execute($params);

        if(explode(' ', $query)[0] == 'SELECT') {
            $data = $statement->fetchAll();
            return $data;
        } else {
            $count = $statement->rowCount();
            if($count)  return true;
            else        return false;
        }
    }

}