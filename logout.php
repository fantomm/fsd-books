<?php
include('./_header.php');

if(!$userId) {
    header("Location: ./index.php");
    exit();
}
?>

    <div id="logoutPage" class="content-wrapper">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h4 class="modal-title">
                            Logout of your Account?
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row text-center">
                            <h5> Are you sure you want to logout? </h5>
                        </div>
                        <div class="row">
                            <form>
                                <div class="col-xs-8">
                                    <input type="checkbox" id="allDevices" name="allDevices" value="allDevices"> Logout of all devices?
                                </div>
                                <div class="col-xs-4 text-right">
                                    <input type="hidden" id="nocsrf" name="nocsrf" value="<?php echo (isset($_SESSION['token'])) ? $_SESSION['token'] : ''; ?>">
                                    <input type="button" id="logoutConfirm" name="logoutConfirm" value="Confirm" class="btn btn-primary">
                                </div>
                            </form>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php include('./_footer.php'); ?>