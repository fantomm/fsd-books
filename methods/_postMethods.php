<?php

/**
 * Routes for method POST
 */
    switch ($_GET['url'])
    {
        case 'auth': // url: /api/auth/logout && /api/auth
            (isset($_GET['type']) && $_GET['type'] == 'logout') ? logoutUser($db) : loginUser($db);
            break;
        case'users': // url: /api/users
            registerUser($db);
            break;
        case'books': // url: /api/books
            createBook($db);
            break;
        default:
            doDefaultAction();
    }


/**
 * Register a new User
 *
 * @param $db
 * @return json response (status code and message)
 */
    function registerUser($db)
    {
        // Get data
        $postBody = file_get_contents("php://input");
        $postBody = json_decode($postBody);

        $formData = array(
            ':username' => isset($postBody->username) ? $postBody->username : null,
            ':email'    => isset($postBody->email)    ? $postBody->email    : null,
            ':password' => isset($postBody->password) ? $postBody->password : null,
            ':role'     => isset($postBody->role)     ? $postBody->role     : null
        );

        $nocsrf = isset($postBody->nocsrf) ? $postBody->nocsrf : null;

        // Validation with Response
        denyAccessIfCsrfTokenInvalid($nocsrf);
        validateUserData($db, $formData);

        // Insert new User and Return Response
        $formData[':password'] = password_hash($formData[':password'], PASSWORD_BCRYPT);
        if(!$db->query('INSERT INTO users VALUES (null, :username, :password, :email, :role)', $formData))
        {
            echo '{ "Error": "No records were affected during this action!" }';
            http_response_code(500); // 500 - Internal Server Error
            exit;
        }
        echo '{ "Success": "User successfully registered" }';
        http_response_code(201); // 201 - Created
    }


/**
 * Login user
 *
 * @param $db
 * @return json response (status code and message)
 */
    function loginUser($db)
    {
        // Get data
        $postBody = file_get_contents("php://input");
        $postBody = json_decode($postBody);

        $formData = array(
            ':username' => $postBody->username,
            ':password' => $postBody->password,
        );
        $nocsrf = isset($postBody->nocsrf) ? $postBody->nocsrf : null;

        // Validation with Response
        denyAccessIfCsrfTokenInvalid($nocsrf);
        validateUsername($db, $formData[':username']);
        validatePassword($db, $formData[':password'], $formData[':username']);

        // Login user
        $token = generateCsrfToken();
        $user_id = $db->query('SELECT id FROM users WHERE username=:username', array(':username' => $formData[':username']))[0]['id'];
        if(!$db->query('INSERT INTO login_tokens VALUES (null, :token, :user_id)', array(':token' => sha1($token), ':user_id' => $user_id)))
        {
            echo '{ "Error": "No records were affected during this action!" }';
            http_response_code(500); // 500 - Internal Server Error
            exit;
        }

        // Set Auth cookie
        setCookies($token);

        // Return Response
        echo '{ "Success": "User Successfully logged in!" }';
        http_response_code(200); // 200 - OK
    }


/**
 * Logout User
 *
 * @param $db
 * @return json response (status code and message)
 */
    function logoutUser($db)
    {
        // Get Data
        $postBody = file_get_contents("php://input");
        $postBody = json_decode($postBody);

        $allDevices = (isset($postBody->allDevices) && $postBody->allDevices == 1) ? true : false;
        $nocsrf = isset($postBody->nocsrf) ? $postBody->nocsrf : null;

        // Validation with Response
        denyAccessIfCsrfTokenInvalid($nocsrf);
        validateAuthCookieToken($db);

        // Logout User by removing the token(s) from db
        if($allDevices)
        { // logout from all devices
            $deleteStatus = $db->query('DELETE FROM login_tokens WHERE user_id=:user_id', array(':user_id' => checkIfLoggedIn($db)));
        }
        else
        { // logout from a single device
            $deleteStatus = $db->query('DELETE FROM login_tokens WHERE token=:token', array(':token' => sha1($_COOKIE['FSDID'])));
        }

        if(!$deleteStatus)
        {
            echo '{ "Error": "No records were affected during this action!" }';
            http_response_code(500); // 500 - Internal Server Error
            exit;
        }

        // Unset Cookies
        unsetCookies();

        // Return Response
        echo '{ "Success": "Loggout was successful!" }';
        http_response_code(200); // 200 - OK
    }


/**
 * Create a new Book
 *
 * @param $db
 * @return json response (status code and message)
 */
    function createBook($db)
    {
        // Get data
        $postBody = file_get_contents("php://input");
        $postBody = json_decode($postBody);

        $formData = array(
            ':title'                => isset($postBody->title)              ? $postBody->title              : null,
            ':author'               => isset($postBody->author)             ? $postBody->author             : null,
            ':published'            => isset($postBody->published)          ? $postBody->published          : null,
            ':language'             => isset($postBody->language)           ? $postBody->language           : null,
            ':language_original'    => isset($postBody->language_original)  ? $postBody->language_original  : null
        );

        $nocsrf = isset($postBody->nocsrf) ? $postBody->nocsrf : null;

        // Validation with Response
        denyAccessIfCsrfTokenInvalid($nocsrf);
        denyAccessIfNotLoggedIn($db);
        validateBookData($formData);

        // Create New Book and Return Response
        $formData[':added'] = date('Y-m-d H:i:s');
        if(!$db->query('INSERT INTO books VALUES (null, :title, :author, :published, :language, :language_original, :added, null)', $formData))
        {
            echo '{ "Error": "No records were affected during this action!" }';
            http_response_code(500); // 500 - Internal Server Error
            exit;
        }
        echo '{ "Success": "Book successfully added!" }';
        http_response_code(201); // 201 - Created
    }


/**
 * Unrecognized Route
 */
    function doDefaultAction()
    {
        echo '{ "Error": "Url does not exist!" }';
        http_response_code(404); // 404 - Not Found
    }