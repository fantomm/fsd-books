<?php

/**
 * Routes for method PUT
 */
    switch ($_GET['url'])
    {
        case 'books': // url: /api/books/{id}
            (isset($_GET['id'])) ? editBook($db, $_GET['id']) : doDefaultAction();
            break;
        default:
            doDefaultAction();
    }


/**
 * Edit a Book
 *
 * @param $db
 * @param string $id
 * @return json response (status code and message)
 */
    function editBook($db, $id = '')
    {
        // Get data
        $postBody = file_get_contents("php://input");
        $postBody = json_decode($postBody);

        $formData = array(
            ':id'                   => $id,
            ':title'                => isset($postBody->title)              ? $postBody->title              : null,
            ':author'               => isset($postBody->author)             ? $postBody->author             : null,
            ':published'            => isset($postBody->published)          ? $postBody->published          : null,
            ':language'             => isset($postBody->language)           ? $postBody->language           : null,
            ':language_original'    => isset($postBody->language_original)  ? $postBody->language_original  : null,
        );

        $nocsrf = isset($postBody->nocsrf) ? $postBody->nocsrf : null;

        // Validation with Response
        denyAccessIfCsrfTokenInvalid($nocsrf);
        denyAccessIfNotLoggedIn($db);
        validateBookData($formData);

        // Update Book and Return Response
        $formData[':modified'] = date('Y-m-d H:i:s');
        if( !$db->query('UPDATE books SET title=:title, author=:author, published=:published, language=:language, language_original=:language_original, modified=:modified WHERE id=:id', $formData))
        {
            echo '{ "Error": "No records were affected during this action!" }';
            http_response_code(500); // 500 - Internal Server Error
            exit;
        }
        echo '{ "Success": "Book successfully changed!" }';
        http_response_code(200); // 200 - OK
    }


/**
 * Unrecognized Route
 */
    function doDefaultAction()
    {
        echo '{ "Error": "Url does not exist!" }';
        http_response_code(404); // 404 - Not Found
    }