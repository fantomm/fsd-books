<?php

/**
 * Routes for method DELETE
 */
    switch ($_GET['url'])
    {
        case 'books': // url: /api/books/{id}/{token}
            (isset($_GET['id']) && isset($_GET['nocsrf'])) ? deleteBook($db, $_GET['id'], $_GET['nocsrf']) : doDefaultAction();
            break;
        default:
            doDefaultAction();
    }


/**
 * Delete a Book
 *
 * @param $db
 * @param string $id
 * @param string $nocsrf CSRF token
 */
    function deleteBook($db, $id = '', $nocsrf = '')
    {
        // Validation with Response
        denyAccessIfCsrfTokenInvalid($nocsrf);
        denyAccessIfNotLoggedIn($db);
        denyAccessIfNotAdmin($db);

        // Delete a Book and Return Response
        if( !$db->query('DELETE FROM books WHERE id=:id', array(':id' => $id)) )
        {
            echo '{ "Error": "No records were affected during this action!" }';
            http_response_code(500); // 500 - Internal Server Error
            exit;
        }
        echo '{ "Success": "Book successfully deleted!" }';
        http_response_code(200); // 200 - OK
    }


/**
 * Unrecognized Route
 */
    function doDefaultAction()
    {
        echo '{ "Error": "Url does not exist!" }';
        http_response_code(404); // 404 - Not Found
    }