<?php

/**
 * Routes for method GET
 */
    switch ($_GET['url'])
    {
        case'books': // url: /api/books/{id} || /api/books/
            (isset($_GET['id'])) ? getBookSingle($db, $_GET['id']) : getBooks($db);
            break;
        default:
            doDefaultAction();
    }


/**
 * Get all Books
 *
 * @param $db
 * @return json response (status code and books data)
 * TODO: refactor/extract parts of code.
 */
    function getBooks($db)
    {
        // Get data
        $per_page = (isset($_GET["per_page"])) ? $_GET["per_page"] : PAGINATION_RECORDS_PER_PAGE;
        $cur_page = (isset($_GET["page"]))     ? $_GET["page"]     : 1;

        $search = (isset($_GET["search"])) ? $_GET["search"] : null;
        $search = (!is_null($search)) ? json_decode($search) : null;
        $search = (array)$search;

        // Do calculations
        $offset = ($cur_page-1) * $per_page;
        $offset   = intval($offset); // for sql injection
        $per_page = intval($per_page); // for sql injection

        // Get Books from DB table
        list($query, $where) = generateGetBooksQuery($search, $offset, $per_page);
        $results = $db->query($query);
        // $data = $db->query("SELECT * FROM books LIMIT :offset, :per_page", array(':offset' => $offset, ':per_page' => $per_page));
        // TODO: there is an pdo error when trying to add Limit in above query.. needs to be changed..

        // Get num of books that are going to be listed
        $queryCount = "SELECT COUNT(id) as count FROM books";
        if(!empty($where)) {
            $queryCount .= " WHERE{$where}";
        }
        $num_records = $db->query($queryCount);
        $num_records = (isset($num_records[0]['count'])) ? $num_records[0]['count'] : 0;


        $data = array('num_records' => $num_records, 'results' => $results, 'per_page' => $per_page, 'cur_page' => $cur_page);

        echo json_encode($data);
        http_response_code(200); // 200 - OK
    }


/**
 * Get Book by ID
 *
 * @param $db
 * @param string $id
 * @return json response (status code and book data)
 */
    function getBookSingle($db, $id = '')
    {
        // Validation with Response
        denyAccessIfNotLoggedIn($db);

        // Return single Book
        echo json_encode($db->query('SELECT * FROM books WHERE id=:id', array(':id' => $id)));
        http_response_code(200); // 200 - OK
    }


/**
 * Unrecognized Route
 */
    function doDefaultAction()
    {
        echo '{ "Error": "Url does not exist!" }';
        http_response_code(404); // 404 - Not Found
    }