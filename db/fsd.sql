-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 28, 2017 at 02:41 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fsd`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `author` varchar(200) DEFAULT NULL,
  `published` int(11) DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `language_original` varchar(100) DEFAULT NULL,
  `added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `published`, `language`, `language_original`, `added`, `modified`) VALUES
(1, 'Oliver Twist', 'Charles Dickens', 1838, 'English', 'English', '2017-07-25 22:55:10', '2017-07-28 14:34:19'),
(2, 'Moby-Dick', 'Herman Melville', 1851, 'English', 'English', '2017-07-25 22:56:52', NULL),
(3, 'War and Peace', 'Leo Tolstoy', 1869, 'English', 'Russian', '2017-07-25 23:00:00', '2017-07-27 13:19:32'),
(4, 'The Great Gatsby', 'F. Scott Fitzgerald', 1925, 'English', 'English', '2017-07-25 23:01:14', '2017-07-27 14:19:10'),
(5, 'Anna Karenina', 'Leo Tolstoy', 1877, 'Russian', 'Russian', '2017-07-25 23:02:24', NULL),
(6, 'Don Quixote', 'Miguel de Cervantes', 1605, 'English', 'Spanish', '2017-07-25 23:04:09', NULL),
(7, 'Odyssey', 'Homer', 1938, 'English', 'Greek', '2017-07-25 23:05:43', NULL),
(8, 'Hamlet', 'William Shakespeare', 1607, 'English', 'English', '2017-07-25 23:07:00', NULL),
(9, 'Divine Comedy', 'Dante Alighieri', 1555, 'Italian', 'Italian', '2017-07-25 23:08:16', NULL),
(10, 'The Iliad', 'Homer', 1598, 'English', 'Greek', '2017-07-25 23:09:29', NULL),
(11, 'The Great Gatsby', 'F. Scott Fitzgerald', 1925, 'English', 'English', '2017-07-25 23:13:34', NULL),
(12, 'title1', 'author1', 1234, 'language1', 'language-original-1', '2017-07-26 16:17:32', NULL),
(13, 'title-2', 'author-2', 1555, 'lang-2', 'lang-original-2', '2017-07-26 16:21:09', '2017-07-28 14:33:11');

-- --------------------------------------------------------

--
-- Table structure for table `login_tokens`
--

CREATE TABLE `login_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `token` varchar(100) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_tokens`
--

INSERT INTO `login_tokens` (`id`, `token`, `user_id`) VALUES
(163, '58180e1cf99cb49d20736f135cff661d26858652', 13);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `type`) VALUES
(1, 'admin'),
(2, 'manager');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role_id`) VALUES
(12, 'admin', '$2y$10$Ew1OgWPP9oTxftUtMmzzo.Nk/w1Xvfn.xvLE8sdD3amEUuAWntGa.', 'admin@admin.com', 1),
(13, 'manager', '$2y$10$HD8qYuQ29I6.Jo7X1gNSEu4K6RJNSttXwz1vEF5Ol6ojHOebBZHCa', 'manager@manager.com', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_tokens`
--
ALTER TABLE `login_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `login_tokens`
--
ALTER TABLE `login_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `login_tokens`
--
ALTER TABLE `login_tokens`
  ADD CONSTRAINT `login_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
