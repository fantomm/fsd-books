<?php
include('./_header.php');

if($userId) {
    header("Location: ./index.php");
    exit();
}
?>
    <link rel="stylesheet" type="text/css" href="./resources/css/loggingPage.css">

    <div id="loginPage" class="content-wrapper">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <h1 class="text-center login-title">Sign in to your account</h1>
                <div class="account-wall">
                    <img class="profile-img" src="./resources/images/user-default.png" alt="user-img">
                    <form class="form-signin">
                        <input type="text" class="form-control" id="username" name="username" value="" placeholder="Username" required autofocus>
                        <input type="password" class="form-control" id="password" name="password" value="" placeholder="Password" required>
                        <input type="hidden" id="nocsrf" name="nocsrf" value="<?php echo (isset($_SESSION['token'])) ? $_SESSION['token'] : ''; ?>">
                        <button class="btn btn-lg btn-primary btn-block" type="button" id="loginBtn" name="login">Sign in</button>
                    </form>
                </div>
                <a href="./register.php" class="text-center new-account">Create an account </a>
            </div>
        </div>
    </div>

<?php include('./_footer.php'); ?>