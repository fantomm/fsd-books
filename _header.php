<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once('./config/parameters.php');
require_once('./classes/DB.php');
require_once('./classes/Login.php');
require_once('./functions.php');

$token  = setupCsrfToken();
$db     = loginToDb();
$userId = checkIfLoggedIn($db);
?>

<!DOCTYPE html>
<html>
<head>
    <title> FSD - books </title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="./resources/images/favicon.png" sizes="16x16">

    <!-- css -->
    <link rel="stylesheet" type="text/css" href="./resources/plugins/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./resources/plugins/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="./resources/css/sticky-footer-navbar.css">
    <link rel="stylesheet" type="text/css" href="./resources/css/style.css">

</head>
<body>

<?php include('./_navigation.php'); ?>

<div class="container-fluid">

    <?php include('./_alert_message.php'); ?>