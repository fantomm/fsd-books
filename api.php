<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once('./config/parameters.php');
require_once('./classes/DB.php');
require_once('./classes/Login.php');
require_once('./functions.php');

$db = loginToDb();

// Get allowed methods for the current route
$route = (isset($_GET['url'])) ? $_GET['url'] : '';
$allowedMethods = getMethodsAllowedForRoute($route);

// Validate request method for the current route
if(!isset($_SERVER['REQUEST_METHOD']) || (isset($_SERVER['REQUEST_METHOD']) && !in_array($_SERVER['REQUEST_METHOD'], $allowedMethods)))
{
    echo'{ "Error": "Method not allowed!" }';
    http_response_code(405); // 405 - Method Not Allowed
    exit;
}

// Proceed to a specific routes file
include('./methods/_'. strtolower($_SERVER['REQUEST_METHOD']) .'Methods.php');