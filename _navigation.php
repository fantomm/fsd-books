<?php
// Current url link
$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>

<nav class="navbar navbar-inverse bg-primary navbar-fixed-top">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="./index.php"><img alt="Brand" src="./resources/images/favicon.png" title="Body and Brains" style="max-width: 30px;"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li class="<?php echo (strpos($url, 'index.php') !== false || strpos($url, 'book_manage.php') !== false || strpos($url, 'book_new.php') !== false) ? 'active' : ''; ?>">
                <a href="./index.php"><span class="glyphicon glyphicon-book"></span> Library</a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="<?php echo (strpos($url, 'register.php') !== false) ? 'active' : ''; ?>"><a href="./register.php"><span class="glyphicon glyphicon-tasks"></span> Register</a></li>
            <?php if($userId): ?>
                <li class="<?php echo (strpos($url, 'logout.php') !== false) ? 'active' : ''; ?>"><a href="./logout.php" class="logout-link"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
            <?php else: ?>
                <li class="<?php echo (strpos($url, 'login.php') !== false) ? 'active' : ''; ?>"><a href="./login.php" class="logout-link"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            <?php endif; ?>
        </ul>
    </div>
</nav>