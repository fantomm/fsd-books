<div id="filter-panel" class="collapse filter-panel">
    <div class="panel panel-default">
        <div class="panel-body bg-warning text-left">
            <form method="get" class="form-inline" role="form">
                <input type="hidden" name="page" value="<?php echo (isset($_GET['page'])) ? $_GET['page'] : 1; ?>">
                <div class="form-group">
                    <label class="filter-col" style="margin-right:0;" for="pref-search">Author:</label>
                    <input type="text" id="author" name="author" class="form-control input-sm" placeholder="- Author -">
                </div><!-- form group [author] -->
                <div class="form-group">
                    <label class="filter-col" style="margin-right:0;" for="pref-perpage">Year Published: </label>
                    <select id="published" name="published" class="form-control">
                        <option value="">- Year -</option>
                        <?php
                        for($i=1000; $i <= date('Y'); ++$i)
                        {
                            echo'<option value="'. $i .'">'. $i .'.</option>';
                        }
                        ?>
                    </select>
                </div> <!-- form group [year] -->
                <div class="form-group">
                    <label class="filter-col" style="margin-right:0;" for="pref-orderby">Order by Year:</label>
                    <select id="filterBy" name="filterBy" class="form-control">
                        <option value="">- No order -</option>
                        <option value="ASC">Ascending</option>
                        <option value="DESC">Descending</option>
                    </select>
                </div> <!-- form group [order by] -->
                <div class="form-group">
                    <button type="submit" id="applyFilterBtn" name="applyFilterBtn" value="true" class="btn btn-primary btn-sm filter-col" style="margin-left:20px;">
                        <span class="glyphicon glyphicon-filter"></span> Apply Filters
                    </button>
                    <a href="./index.php<?php echo (isset($_GET['page'])) ? '?page='.$_GET['page'] : ''; ?>" class="btn btn-danger btn-sm filter-col" style="margin-left:20px;">
                        <span class="glyphicon glyphicon-refresh"></span> Reset Filters
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
<button type="button" class="btn btn-sm btn-primary pull-left" data-toggle="collapse" data-target="#filter-panel" style="margin-bottom: 10px;">
    <span class="glyphicon glyphicon-cog"></span> Advanced Filters
</button>