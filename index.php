<?php include('./_header.php'); ?>

    <div class="panel panel-primary panel-table-holder">
        <div class="panel-heading">Books</div>

        <div id="filters-wrapper" class="text-right">

            <?php include('_search_filters.php'); ?>

            <?php if($userId): ?>
                <a href="./book_new.php" type="button" class="btn btn-success btn-sm"> <span class="glyphicon glyphicon-plus"></span> Add New Book</a>
            <?php endif; ?>
        </div>

        <table class="table table-bordered table-striped table-hover table-books-list">
            <thead>
                <tr>
                    <th class="bg-primary text-center">Title</th>
                    <th class="bg-primary text-center">Author</th>
                    <th class="bg-primary text-center">Published</th>
                    <th class="bg-primary text-center">Language</th>
                    <th class="bg-primary text-center">Orig.Lang.</th>
                    <th class="bg-primary text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <!-- Table body part -->
            </tbody>
        </table>
        <input type="hidden" id="currPage" value="booksPage" />
        <input type="hidden" id="userLogged" value="<?php echo($userId) ? true : false; ?>" />

        <div class="pagination_holder text-right bg-info">
            <div id="pagination_links">
                <!-- pagination here -->
            </div>
        </div>

    </div>

<?php include('./_footer.php'); ?>