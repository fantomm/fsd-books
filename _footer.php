    </div> <!-- // div.container -->

    <footer class="footer navbar-inverse">
        <span class="text-muted">
            FSD - Book Library &reg; <a href="https://www.linkedin.com/in/milan-ra%C5%A1lji%C4%87-70084560" target="_blank">Milan Rasljic</a>
        </span>
    </footer>

    <!-- JS -->
    <script src="./resources/js/jquery-3.2.1.min.js"></script>
    <script src="./resources/plugins/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="./resources/js/api.js"></script>

</body>
</html>