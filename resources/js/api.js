$(function()
{
    /**
     * Login user
     */
    $('#loginPage').on('click', '#loginBtn', function()
    {
        // Get data
        var username = $('#username').val();
        var password = $('#password').val();
        var nocsrf   = $('#nocsrf').val();

        var data = '{ "username": "'+ username +'", "password": "'+ password +'", "nocsrf": "'+ nocsrf +'" }';

        // Call API
        $.ajax({
            type: "POST",
            url: "./api.php?url=auth",
            processData: false,
            contentType: "application/json",
            data: data,
            success: function (response) {
                window.location.replace("index.php");
            },
            error: function (response) {
                showAlertMessage(response, 'Error');
            }
        });
    });


    /**
     * Register new user
     */
    $('#registerPage').on('click', '#createAccount', function()
    {
        // Get data
        var username = $('#username').val();
        var password = $('#password').val();
        var email    = $('#email').val();
        var role     = $('#role').val();
        var nocsrf   = $('#nocsrf').val();

        var data = '{ "username": "'+ username +'", "password": "'+ password +'", "email": "'+ email +'", "role": "'+ role +'", "nocsrf": "'+ nocsrf +'" }';

        // Call API
        $.ajax({
            type: "POST",
            url: "./api.php?url=users",
            processData: false,
            contentType: "application/json",
            data: data,
            success: function (response) {
                // window.location.replace("login.php");
                showAlertMessage(response, 'Success');
                $(':input').not('input[type=button], input[name=nocsrf]').val('');
            },
            error: function (response) {
                showAlertMessage(response, 'Error');
            }
        });
    });


    /**
     * Logout user
     */
    $('#logoutPage').on('click', '#logoutConfirm', function()
    {
        // Get data
        var allDevices = 0;
        if ($('#logoutPage #allDevices').is(':checked')) {
            allDevices = 1;
        }
        var nocsrf = $('#nocsrf').val();
        var data   = '{ "allDevices": "'+ allDevices +'", "nocsrf": "'+ nocsrf +'" }';

        // Call API
        $.ajax({
            type: "POST",
            url: "./api.php?url=auth&type=logout",
            processData: false,
            contentType: "application/json",
            data: data,
            success: function (response) {
                window.location.replace("index.php");
            },
            error: function (response) {
                showAlertMessage(response, 'Error');
            }
        });
    });


    /**
     * List all books on page load
     */
    if(typeof $('#currPage').val() !== 'undefined' && $('#currPage').val() == 'booksPage')
    {
        // Get Page from url:
        var url_string = window.location.href;
        var url = new URL(url_string);
        var page  = url.searchParams.get("page");

        if(typeof page == 'undefined' || page == null || page == 0) {
            page = 1;
        }

        // Get search params and populate inputs
        var search = {
            'author'    : url.searchParams.get("author"),
            'published' : url.searchParams.get("published"),
            'filterBy'  : url.searchParams.get("filterBy")
        };

        $('#author').val(search['author']);
        $('#published').val(search['published']);
        $('#filterBy').val(search['filterBy']);

        if(url.searchParams.get("applyFilterBtn") && (search['author'].length > 0 || search['published'].length > 0)) {
            page = 1;
        }

        // Get books
        listAllBooks(page, search);
    }

    function listAllBooks(page, search)
    {
        var loadingRow = '<tr><td colspan="6"><img src="./resources/images/loading.gif" alt="loading..." ></td></tr>';
        $('.table-books-list tbody').html(loadingRow);

        var searchEncoded = JSON.stringify(search);

        // Call API
        $.ajax({
            type: "GET",
            url: "./api.php?url=books&page="+page+'&search='+searchEncoded,
            // url: "./api.php?url=books&page="+page+encoded,
            processData: false,
            contentType: "application/json",
            data: '',
            success: function (response) {
                var books = $.parseJSON( response );

                if(books['results'].length > 0)
                {
                    // Create pagination links
                    createPaginationLinks(books, search);

                    // Populate table with Books
                    $('.table-books-list tbody').html('');

                    $.each( books['results'], function( key, value )
                    {
                        var row = '<tr>'+
                            '<td>'+ value.title +'</td>'+
                            '<td>'+ value.author +'</td>'+
                            '<td class="text-center">'+ value.published +'.</td>'+
                            '<td class="text-center">'+ value.language +'</td>'+
                            '<td class="text-center">'+ value.language_original +'</td>';

                            if(typeof $('#userLogged').val() !== 'undefined' && $('#userLogged').val() == '1') {
                                row += '<td class="text-center"><a href="./book_manage.php?id='+ value.id +'" type="button" class="btn btn-info btn-xs"> <span class="glyphicon glyphicon-edit"></span> manage</a></td>';
                            } else {
                                row += '<td></td>';
                            }
                        row += '</tr>';

                        $('.table-books-list tbody').append(row);
                    });
                }
                else
                {
                    $('.table-books-list tbody').html('<tr><td colspan="6">No books available.</td></tr>');
                }

            },
            error: function (response) {
                $('.table-books-list tbody').html('<tr><td colspan="6">No books available.</td></tr>');
                showAlertMessage(response, 'Error');
            }
        });
    }


    /**
     * Show Book Manage page
     */
    if(typeof $('#currPage').val() !== 'undefined' && $('#currPage').val() == 'bookManagePage')
    {
        // Get book ID from url:
        var url_string = window.location.href;
        var url = new URL(url_string);
        var id  = url.searchParams.get("id");

        // Call API
        $.ajax({
            type: "GET",
            url: "./api.php?url=books&id=" + id,
            processData: false,
            contentType: "application/json",
            data: '',
            success: function (response) {
                var book = $.parseJSON(response);

                if(book.length > 0)
                {
                    // Populate Single Book data to form
                    $('.table-books-list tbody').html('');

                    $('#title').val(book[0]['title']);
                    $('#author').val(book[0]['author']);
                    $('#published').val(book[0]['published']);
                    $('#language').val(book[0]['language']);
                    $('#language_original').val(book[0]['language_original']);
                    $('#bookNum').val(book[0]['id']);

                    if(typeof $('#userLogged').val() == 'undefined' || (typeof $('#userLogged').val() !== 'undefined' && $('#userLogged').val() == '0')) {
                        $('.modal-footer').html('No actions permitted!');
                    }
                }
                else
                {
                    $('.modal-body').html('<p>This book does not exist! <a href="./book_new.php">[add new book]</a></p>');
                    // window.location.replace("index.php");
                }

            },
            error: function (response) {
                showAlertMessage(response, 'Error');
            }
        });
    }


    /**
     * Show Book Add New Page
     */
    $('#bookNewPage').on('click', '#addBook', function()
    {
        // Get data
        var title = $('#title').val();
        var author = $('#author').val();
        var published = $('#published').val();
        var language = $('#language').val();
        var language_original = $('#language_original').val();
        var nocsrf = $('#nocsrf').val();

        var data = '{ "title": "'+ title +'", "author": "'+ author +'", "published": "'+ published +'", "language": "'+ language +'", "language_original": "'+ language_original +'", "nocsrf": "'+ nocsrf +'" }';

        // Call API
        $.ajax({
            type: "POST",
            url: "./api.php?url=books",
            processData: false,
            contentType: "application/json",
            data: data,
            success: function (response) {
                showAlertMessage(response, 'Success');
                $(':input').not('input[type=button], input[name=nocsrf]').val('');
            },
            error: function (response) {
                showAlertMessage(response, 'Error');
            }
        });
    });


    /**
     * Save changes to single book
     */
    $('#bookManagePage').on('click', '#saveBookChange', function()
    {
        // Get data
        var title = $('#title').val();
        var author = $('#author').val();
        var published = $('#published').val();
        var language = $('#language').val();
        var language_original = $('#language_original').val();
        var id = $('#bookNum').val();
        var nocsrf = $('#nocsrf').val();

        var data = '{ "id": "'+ id +'", "title": "'+ title +'", "author": "'+ author +'", "published": "'+ published +'", "language": "'+ language +'", "language_original": "'+ language_original +'", "nocsrf": "'+ nocsrf +'" }';

        // Call API
        $.ajax({
            type: "PUT",
            url: "./api.php?url=books&id=" + id,
            processData: false,
            contentType: "application/json",
            data: data,
            success: function (response) {
                showAlertMessage(response, 'Success');
            },
            error: function (response) {
                showAlertMessage(response, 'Error');
            }
        });
    });


    /**
     * Delete Book
     */
    $('#bookManagePage').on('click', '#deleteBook', function()
    {
        // Get data
        var id = $('#bookNum').val();
        var nocsrf = $('#nocsrf').val();

        // Call API
        $.ajax({
            type: "DELETE",
            url: "./api.php?url=books&id=" + id +"&nocsrf=" + nocsrf,
            processData: false,
            contentType: "application/json",
            data: '',
            success: function (response) {
                $('.modal-body').html('<p>Book Deleted! <a href="./book_new.php">[add new book]</a></p>');
                $('.modal-footer').html('');
                showAlertMessage(response, 'Success');
            },
            error: function (response) {
                showAlertMessage(response, 'Error');
            }
        });
    });

}); // END: $(function()...


/////////////////////////////// Functions ///////////////////////////////

/**
 * Concat and Show alert message
 *
 * @param response
 * @param status
 */
    function showAlertMessage(response, status)
    {
        // Get data
        if(status == 'Success') {
            var json_x  = $.parseJSON(response);
            var icon    = 'glyphicon glyphicon-ok';
            var alert   = 'alert-success';
            var message = json_x[status];
        }
        else if(status == 'Error') {
            var json_x  = $.parseJSON(response.responseText);
            var icon    = 'glyphicon glyphicon-exclamation-sign';
            var alert   = 'alert-danger';
            var message = json_x[status];
        }
        else {
            var icon    = 'glyphicon glyphicon-info-sign';
            var alert   = 'alert-info';
            var message = 'FSD - Book Library!';
        }

        // Concat alert
        var alertMsg = $('.alert-message');
        alertMsg.attr('class', 'alert alert-message '+alert);
        alertMsg.find('span.glyphicon').attr('class', icon);
        alertMsg.find('span.messageAlert').text(message);
        alertMsg.slideDown();

        // Show alert
        setTimeout(function() {
            alertMsg.slideUp(1000);
        }, 6000);
    }


/**
 * Creating Pagination links for Books List view
 * @param books
 * @param search
 * @returns {boolean}
 *
 * TODO: implement disabling of first/last buttons if current page already at the first/last position.
 * TODO: maybe implement changing pages without refreshing the page - with jquery call instead.
 */
    function createPaginationLinks(books, search)
    {
        if(books['num_records'] > 0)
        {
            var lastPage = Math.ceil(books['num_records'] / books['per_page']);
            var pagination = '';
            if(lastPage > 1)
            {
                // Generate search urls for links
                if((search['author'] != null && search['author'].length > 0) ||
                    (search['published'] != null && search['published'].length > 0) ||
                    (search['filterBy'] != null && search['filterBy'].length > 0))
                {
                    var searchUrl = '&author=' + search['author'] + '&published=' + search['published'] + '&filterBy=' + search['filterBy'];
                } else {
                    var searchUrl = '';
                }

                // Create links
                pagination += '<nav aria-label="Books navigation">'+
                                '<ul class="pagination">'+
                                    '<li class="page-item">'+
                                        '<a class="page-link" href="./index.php?page=1'+searchUrl+'" aria-label="Previous" title="first">'+
                                            '<span aria-hidden="true">&laquo;</span>'+
                                            '<span class="sr-only">First</span>'+
                                        '</a>'+
                                    '</li>';

                                    var active = '';
                                    for ( var i=1; i <= lastPage; i++)
                                    {
                                        active = ((books['cur_page'] == i) ? "active" : '');
                                        pagination += '<li class="page-item '+ active +'"><a class="page-link" href="./index.php?page='+ i + searchUrl +'" title="'+ i +'">'+ i +'</a></li>';

                                    }

                pagination +=       '<li class="page-item">'+
                                        '<a class="page-link" href="./index.php?page='+ lastPage + searchUrl +'" aria-label="Next" title="last">'+
                                            '<span aria-hidden="true">&raquo;</span>'+
                                            '<span class="sr-only">Last</span>'+
                                        '</a>'+
                                    '</li>'+
                                '</ul>'+
                            '</nav>';
            }
            $('#pagination_links').html(pagination);
        }
        return false;
    }