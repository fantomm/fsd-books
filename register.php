<?php include('./_header.php'); ?>

    <div id="registerPage" class="content-wrapper">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h4 class="modal-title">
                            Create an account
                        </h4>
                    </div>
                    <div class="modal-body">
                        <form novalidate="novalidate" id="formSignUp">
                            <div class="form-group">
                                <label class="control-label" for="username">Username</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                    <input class="form-control" placeholder="Username" name="username" id="username" type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="password">Password</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                    <input class="form-control" placeholder="Password" name="password" id="password" type="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="email">Email</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                                    <input class="form-control" placeholder="email@example.com" name="email" id="email" type="email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="role">Role</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-cog"></span></span>
                                    <select class="form-control" id="role" name="role">
                                        <option value="">Select Role</option>
                                        <option value="1">Admin</option>
                                        <option value="2">Manager</option>
                                    </select>
                                </div>
                            </div>

                            <input type="hidden" id="nocsrf" name="nocsrf" value="<?php echo (isset($_SESSION['token'])) ? $_SESSION['token'] : ''; ?>">
                            <input type="button" id="createAccount" name="createAccount" value="Create an account" class="btn btn-primary">
                        </form>

                    </div>
                    <div class="modal-footer bg-info">
                        <small>Already a member? <a class="alreadySignUp" href="./login.php">Login here</a></small>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('./_footer.php'); ?>