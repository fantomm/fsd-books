<?php include('./_header.php'); ?>

<?php
if(!$userId) {
    header("Location: ./index.php");
    exit();
}
?>

<div id="bookManagePage" class="content-wrapper">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Manage Book</div>
                    </div>
                </div>
                <div class="modal-body">
                    <form novalidate="novalidate">
                        <div class="form-group">
                            <label class="control-label" for="title">Title</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-book"></span></span>
                                <input class="form-control" placeholder="Title" name="title" id="title" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="author">Author</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                <input class="form-control" placeholder="Author" name="author" id="author" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="published">Year Published</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input class="form-control" placeholder="Year Published" name="published" id="published" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="language">Language</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-text-color"></span></span>
                                <input class="form-control" placeholder="Language" name="language" id="language" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="language_original">Original Language</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-text-background"></span></span>
                                <input class="form-control" placeholder="Original Language" name="language_original" id="language_original" type="text">
                            </div>
                        </div>

                        <div class="modal-footer bg-info">
                            <input type="hidden" id="bookNum" name="bookNum" value="">
                            <input type="hidden" id="nocsrf" name="nocsrf" value="<?php echo (isset($_SESSION['token'])) ? $_SESSION['token'] : ''; ?>">
                            <input type="button" id="saveBookChange" name="saveBookChange" value="Save Changes" class="btn btn-success pull-right">
                            <input type="button" id="deleteBook" name="deleteBook" value="Delete Book" class="btn btn-danger pull-left">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="currPage" value="bookManagePage" />
    <input type="hidden" id="userLogged" value="<?php echo($userId) ? true : false; ?>" />
</div>

<?php include('./_footer.php'); ?>
