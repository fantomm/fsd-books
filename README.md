**FSD** test project
=====================

**Author:** Milan Rasljic (rasljic.milan@gmail.com)

**Technologies used:**  
	- Php  
	- MySQL  
	- Html  
	- Css  
	- Jquery  
	- Ajax  
	- Bootstrap  
	
**User roles:**  
	- admin (permissions to delete a book)  
	- manager  

**Login credentials for current users:**  
	- Admin: admin / admin  
	- Manager: manager / manager  

**Database:**  
	- One can find an exported MySQL database here: ./db/fsd.sql  
	- HOST: localhost (127.0.0.1)  
	- USER: root  
	- PASS: /  
	- DB: fsd (utf8_general_ci)  

**Done:**  
	- REST API  
	- Registratin, Authentication, Authorization  
	- CRUD for books (with permissions)  
	- Bonus:  
		- Search by author and by year  
		- Added order by year  
	- Frontend with Bootstrap  
	- Error handling  
	- Bonus questions answered in the email.  
	- Project set up on bitbucket.  
	- Etc.  

**Description:**  
	- The project is done with no framework at this time (clean php).  
	- Setting up the project is done just by getting the files and setting up the database.  
	- Enabled cookies needed for authentication.  
	- User roles: difference between an Admin and Manager role is that Admin has permission to delete a book while Manager has not. A delete button is not removed from the view (when the manager is logged in) just for testing purposes and for showing the error. Even though it can also be done with Postman.  

**Permissions:**  
	- Library (index) page: all  
	- Register page: all  
	- Login page: all  
	- Logout page: admin / manager  
	- Add New Book page: admin / manager  
	- Manage Book page: admin / manager  
	- Delete Book action: admin  

**Additional info:**  
	- On logout page there is checkbox "Logout of all devices". That is used when one wants to remove all his tokens from the database. That would mean that if he is logged in on several device - he will be logged out from all of them. If the checkbox is not clicked the he will be logged only from this one device.  

**Improvements:**  
	- Filter Year Published should have From and To inputs (range filter).  
	- Adding Sortable Columns to the table.  
	- Better UI design (html, css).  
	- Confirmation before delete.  
	- More input validation rules.  
	- Remove column Action on index page (in the table with books) when a user is not logged in.  
	- Maybe make everything run without refreshing the page (jquery, ajax, angular, react, etc).  
	- A little bit more code refactoring/extracting.  
	- Writing a better README file using all that .md offers.  
	- Add tests to the project.  
	- Etc.  